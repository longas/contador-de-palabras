import easygui as eg

text_file = eg.fileopenbox() #Seleccionar un fichero
text_open = open(text_file) #Abrir el fichero
text_read = text_open.read() #Leer el fichero
text_words = text_read.split() #Array con las palabras

#Creamos un diccionario con las veces que sale cada palabra
words = {}
for word in text_words:
	words[word] = words.get(word, 0) + 1

#Creamos un string con todas las palabras
palabras_string = "Hay " + str(len(text_words)) + " palabras."
for word, times in words.items():
	palabras_string += "\n" + word + " : " + str(times)

#Creamos la ventana que muestre el string anteriormente creado
eg.textbox("Veces que sale una palabra", "Palabras", palabras_string)