Contador de palabras
====================
Gabriel Longás
--------------

Ejecutar **python palabras.py** en consola. Se abrirá una ventana en la que podrás elegir cualquier archivo .txt para que el programa muestre la cantidad de palabras que hay y las veces que sale cada una. Se ha proporcionado lorem.txt para poder comprobarlo rapidamente.